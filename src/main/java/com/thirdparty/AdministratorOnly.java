package com.thirdparty;

import java.util.Map;
import java.util.function.Predicate;
import com.kubiak.filter.Filter;

/**
 * An example of how {@link Filter} functionality can be extended, 
 * by a new type of filter that returns true only if user has administrator role.
 *
 */
public class AdministratorOnly extends Filter{
	
	/**
	 * Predicate for testing whether user resource has administrator role
	 */
	private static Predicate<Map<String, String>> administratorPredicate =
			(m -> m.containsKey("role") ? m.get("role").equalsIgnoreCase("administrator") : false);
	

	/**
	 * New definitioin of {@link Filter#matches(Map)} function that additionally
	 * validates the {@link #administratorPredicate}
	 * @param resource
	 * @return
	 */
	@Override
	public boolean matches(Map<String, String> resource) {
		return administratorPredicate.test(resource) && super.matches(resource);
	}
	

}
