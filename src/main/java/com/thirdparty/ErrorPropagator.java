package com.thirdparty;

import java.util.function.Predicate;

import com.kubiak.filter.Filter;
import com.kubiak.filter.WrappedPredicate;

/**
 * An example of how {@link Filter} functionality can be extended, 
 * by a new type of filter that propagates {@link NumberFormatException} errors.
 */
public class ErrorPropagator<T> extends WrappedPredicate<T> {
	
	Filter filter;
	
	public ErrorPropagator(Filter filter, Predicate<T> predicate) {
		super(predicate);
		this.filter = filter;
	}

	/**
	 * New implementation of {@link Predicate#test(Object)} method that will not swallow
	 * {@link NumberFormatException}
	 */
	@Override
	public boolean test(T t) {
		try {
			return super.test(t);
		}catch(NumberFormatException nfe) {
			throw nfe;
		}catch(Error e) {
			return filter.isDefaultWhenError();
		}
	}

}
