package com.thirdparty;

import com.kubiak.filter.Filter;

/**
 * An example of how {@link Filter} functionality can be extended, 
 * by a new type of filter that provides new functions for checking user's age
 *
 */
public class SimpleAgeFilter extends Filter{
	
	/** 
	 * The name of the property to retrieve
	 */
	private final static String property = "age";
	
	/**
	 * Adds new predicate validating that user is younger then age provided
	 * @param age
	 */
	public void youngerThan(int age) {
		propertyLessThan(property, age);
	}
	
	/**
	 * Adds new predicate validating that user is older then age provided
	 * @param age
	 */
	public void olderThan(int age) {
		propertyGreaterThan(property, age);
	}
	
	/**
	 * Adds new predicate validating that user's age is as provided
	 * @param age
	 */
	public void ageEqual(int age) {
		propertyEqual(property, age);
	}

}
