package com.thirdparty;

import com.kubiak.filter.Filter;

/**
 * An example of how {@link Filter} functionality can be extended, 
 * by a new type of filter that ignores case when comparing string properties
 *
 */
public class FilterIgnoreCase extends Filter{
	
	public FilterIgnoreCase() {
		super();
		super.setValueRetriever( (m, property) -> m.get(property).toLowerCase());
	}
	
	public void propertyEqual(String property, String value) {
		super.propertyEqual(property, value.toLowerCase());
	}
	
	public void propertyLessThan(String property, String value) {
		super.propertyLessThan(property, value.toLowerCase());
	}
	
	public void propertyGreaterThan(String property, String value) {
		super.propertyGreaterThan(property, value.toLowerCase());
	}
	
	

}
