package com.kubiak.filter;

/**
 * Interface used by {@link Filter} to modify value of retrieved property before validating predicates
 * @author Tomasz Kubiak
 */
public interface ValueRetriever<T> {

	public String getProperty(T m, String property);
}
