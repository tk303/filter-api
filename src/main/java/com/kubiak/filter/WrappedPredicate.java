package com.kubiak.filter;

import java.util.function.Predicate;

/**
 * Abstract class that can be used to modify default filters behaviour
 * @author Tomasz Kubiak
 *
 * @param <T>
 */
public abstract class WrappedPredicate<T> implements Predicate<T> {
	private final Predicate<T> predicate;
	public WrappedPredicate(Predicate<T> predicate) {
		this.predicate = predicate;
	}
	
	@Override
	public boolean test(T t) {
		return predicate.test(t);
	}
}
