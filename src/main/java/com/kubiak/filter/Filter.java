package com.kubiak.filter;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;
import java.util.function.Predicate;

/**
 * Filter API which provides the functionality to determine whether or not a resource 
 * matches a given set of criteria represented as a {@link Predicate}
 * 
 * @author Tomasz Kubiak
 */
public class Filter {
	
	/** Holds this Filter's {@link Predicate}s  */
	private Predicate<Map<String, String>> filterPredicate = null;
	
	/** Default behaviour when property is missing.
	 *  Value that is returned when property is missing for given predicate. */
	private boolean defaultWhenMissing = true;
	
	/** Default behaviour when predicate test throws an error */
	private boolean defaultWhenError = true;
	
	/** Default behaviour when no predicate has been set. */
	private boolean defaultWhenNoPredicate= true;
	
	/** Should the value of the predicate be negated when testing (matching) */
	private boolean negate = false;
	
	
	/** Wrapper class. 
	 * Can be used to modify default {@link Predicate#test(Object)} behaviour */
	@SuppressWarnings("rawtypes")
	private Class<? extends WrappedPredicate> wrapperClass = MyWrappedPredicate.class;
	
	/** Function that allows to modify the retrieved value of a property.
	 * Default implementation does not make any change. */
	private ValueRetriever<Map<String, String>> valueRetriever = (m, property) -> m.get(property);


	/**
	 * Validates whether a filter matches a given resource
	 * @param resource
	 * @return
	 */
	public boolean matches(Map<String, String> resource) {
		if(filterPredicate == null) {
			return negate ? !defaultWhenNoPredicate : defaultWhenNoPredicate;
		}
		else {
			return negate ? !filterPredicate.test(resource) : filterPredicate.test(resource);
		}
	}
	
	
	
	/**
	 * Adds a new predicate for testing whether property is set to true.
	 * {@link Boolean#parseBoolean(String)} function is used to parse string to boolean
	 * @param property to test
	 */
	public void propertyIsTrue(String property) {
		addPredicate(m -> m.containsKey(property) ? Boolean.parseBoolean(getProperty(m, property)) : defaultWhenMissing);
	}

	/**
	 * Adds a new predicate for testing whether property is set to false
	 * {@link Boolean#parseBoolean(String)} function is used to parse string to boolean
	 * @param property to test
	 */
	public void propertyIsFalse(String property) {
		addPredicate(m -> m.containsKey(property) ? !Boolean.parseBoolean(getProperty(m, property)) : defaultWhenMissing);
	}
	
	/**
	 * Adds a new predicate for testing whether property is present
	 * @param property to test
	 */
	public void propertyIsPresent(String property) {
		addPredicate(m -> m.containsKey(property));
	}
	
	/**
	 * Adds a new predicate for testing whether property is equal 
	 * @param property to test
	 * @param comparable element to be equal that is comparable with String.
	 */
	public void propertyEqual(String property, Comparable<String> comparable) {
		addPredicate(m -> m.containsKey(property) ? comparable.compareTo(getProperty(m, property)) == 0 : defaultWhenMissing);
	}
	
	/**
	 * Adds a new predicate for testing whether property is less than
	 * @param property to test
	 * @param comparable element to be equal that is comparable with String.
	 */
	public void propertyLessThan(String property, Comparable<String> comparable) {
		addPredicate(m -> m.containsKey(property) ? comparable.compareTo(getProperty(m, property)) < 0 : defaultWhenMissing);
	}
	
	/**
	 * Adds a new predicate for testing whether property is greater than
	 * @param property to test
	 * @param comparable element to be equal that is comparable with String.
	 */
	public void propertyGreaterThan(String property, Comparable<String> comparable) {
		addPredicate(m -> m.containsKey(property) ? comparable.compareTo(getProperty(m, property)) > 0 : defaultWhenMissing);
	}
	
	
	/**
	 * Adds a new predicate for testing whether property is equal an int value
	 * {@link Integer#parseInt(String)} function is used to parse string to int
	 * @param property to test
	 * @param value to compare
	 */
	public void propertyEqual(String property, int value) {
		propertyEqual(property, new Comparable<String>() {
			@Override
			public int compareTo(String key) {
				int i = Integer.parseInt(key);
				return Integer.compare(i, value);
			}
		});
	}
	
	/**
	 * Adds a new predicate for testing whether property is less than an int value
	 * {@link Integer#parseInt(String)} function is used to parse string to int
	 * @param property to test
	 * @param value to compare
	 */
	public void propertyLessThan(String property, int value) {
		propertyLessThan(property, new Comparable<String>() {
			@Override
			public int compareTo(String key) {
				int i = Integer.parseInt(key);
				return Integer.compare(i, value);
			}
		});
	}
	
	/**
	 * Adds a new predicate for testing whether property is greater than an int value
	 * {@link Integer#parseInt(String)} function is used to parse string to int
	 * @param property to test
	 * @param value to compare
	 */
	public void propertyGreaterThan(String property, int value) {
		propertyGreaterThan(property, new Comparable<String>() {
			@Override
			public int compareTo(String key) {
				int i = Integer.parseInt(key);
				return Integer.compare(i, value);
			}
		});
	}
	
	/**
	 * Tests whether specified property matches a string pattern
	 * @param property
	 * @param pattern
	 */
	public void propertyMatches(String property, String pattern) {
		addPredicate(m -> m.containsKey(property) ? defaultWhenMissing :  getProperty(m, property).matches(pattern) );
	}
	
	
	/**
	 * Joins two filters with an AND operation 
	 * @param filter to join
	 * @return new Filter instance
	 */
	public Filter and(Filter filter) {
		Filter returnFilter = new Filter();
		
		Predicate<Map<String, String>> thisPredicate = 
			negate ? this.filterPredicate.negate() : this.filterPredicate;
		
		Predicate<Map<String, String>> otherPredicate = 
				filter.isNegate() ? filter.filterPredicate.negate() : filter.filterPredicate;
		
		returnFilter.addAndPredicate(thisPredicate.and(otherPredicate));
		return returnFilter;
	}
	
	/**
	 * Joins two filters with an OR operation 
	 * @param filter to join
	 * @return new Filter instance
	 */
	public Filter or(Filter filter) {
		Filter returnFilter = new Filter();
		
		Predicate<Map<String, String>> thisPredicate = 
				negate ? this.filterPredicate.negate() : this.filterPredicate;
			
		Predicate<Map<String, String>> otherPredicate = 
					filter.isNegate() ? filter.filterPredicate.negate() : filter.filterPredicate;
		
		returnFilter.addOrPredicate(thisPredicate.or(otherPredicate));
		return returnFilter;
	}
	
	/**
	 * Adds new predicate with default operation as AND
	 * @param newPredicate to add
	 */
	public void addPredicate(Predicate<Map<String, String>> newPredicate) {
		addAndPredicate(newPredicate);
	}
	
	/**
	 * Adds  new {@link Predicate} joint as AND {@link Predicate#and(Predicate)}
	 * @param newPredicate
	 */
	public void addAndPredicate(Predicate<Map<String, String>> newPredicate) {
		addWrappedPredicate(newPredicate, true);
	}
	
	
	/**
	 * Adds  new {@link Predicate} joint as OR {@link Predicate#or(Predicate)}
	 * @param newPredicate
	 */
	public void addOrPredicate(Predicate<Map<String, String>> newPredicate) {
		addWrappedPredicate(newPredicate, false);
	}
	
	/**
	 * Adds new predicate with either AND or OR operation.
	 * Added predicate will be wrapped in an implementation of {@link WrappedPredicate} class
	 * specified in {@link #wrapperClass} 
	 * @param predicate to add
	 * @param addAsAnd when true add as AND, when false add as OR
	 */
	@SuppressWarnings("unchecked")
	private void addWrappedPredicate(Predicate<Map<String, String>> predicate, boolean addAsAnd) {
		WrappedPredicate<Map<String, String>> newPredicate;
		try {
			newPredicate = wrapperClass.getConstructor(Filter.class, Predicate.class).newInstance(this, predicate);
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e) {
			newPredicate = new MyWrappedPredicate<>(predicate);
		}
		if(filterPredicate == null) {
			filterPredicate = newPredicate;
		}
		else {
			if(addAsAnd) {
				filterPredicate = filterPredicate.and(newPredicate);
			} else {
				filterPredicate = filterPredicate.or(newPredicate);
			}
		}
	}
	
	/**
	 * Negates the result of {@link matches} method.
	 * Cannot used build in Predicate's {@link Predicate#negate()} method 
	 * as this could give wrong result if negate wasn't called as the last method on filter before matching.
	 */
	public void negate() {
		this.negate = true;
	}
	
	/**
	 * Resets the value of {@link #negate}
	 */
	public void resetNegate() {
		this.negate = false;
	}
	
	
	/**
	 * Default implementation of a {@link WrappedPredicate} that catches all exceptions
	 * @author Tomasz Kubiak
	 *
	 * @param <T>
	 */
	public class MyWrappedPredicate<T> extends WrappedPredicate<T> {
		public MyWrappedPredicate(Predicate<T> predicate) {
			super(predicate);
		}
		
		@Override
		public boolean test(T t) {
			try {
				return super.test(t);
			}catch(NumberFormatException | Error e) {
				return defaultWhenError;
			}
		}
	}
	
	/**
	 * Private method used to retrieve property.
	 * Allows to customize the the value of the property that is being retrieved
	 * as well as the key
	 * @param m
	 * @param property
	 * @return
	 */
	private String getProperty(Map<String, String> m, String property) {
		return valueRetriever.getProperty(m, property);
	}

	/* ********************       Getters and Setters   **************************** */
	
	/**
	 * @return the defaultWhenMissing
	 */
	public boolean isDefaultWhenMissing() {
		return defaultWhenMissing;
	}

	/**
	 * @param defaultWhenMissing the defaultWhenMissing to set
	 */
	public void setDefaultWhenMissing(boolean defaultWhenMissing) {
		this.defaultWhenMissing = defaultWhenMissing;
	}

	/**
	 * @return the defaultWhenError
	 */
	public boolean isDefaultWhenError() {
		return defaultWhenError;
	}

	/**
	 * @param defaultWhenError the defaultWhenError to set
	 */
	public void setDefaultWhenError(boolean defaultWhenError) {
		this.defaultWhenError = defaultWhenError;
	}

	/**
	 * @return the defaultWhenNoPredicate
	 */
	public boolean isDefaultWhenNoPredicate() {
		return defaultWhenNoPredicate;
	}

	/**
	 * @param defaultWhenNoPredicate the defaultWhenNoPredicate to set
	 */
	public void setDefaultWhenNoPredicate(boolean defaultWhenNoPredicate) {
		this.defaultWhenNoPredicate = defaultWhenNoPredicate;
	}
	

	/**
	 * @return the negate
	 */
	public boolean isNegate() {
		return negate;
	}

	/**
	 * @return the wrapperClass
	 */
	@SuppressWarnings("rawtypes")
	public Class<? extends WrappedPredicate> getWrapperClass() {
		return wrapperClass;
	}

	/**
	 * @param wrapperClass the wrapperClass to set
	 */
	@SuppressWarnings("rawtypes")
	public void setWrapperClass(Class<? extends WrappedPredicate> wrapperClass) {
		this.wrapperClass = wrapperClass;
	}


	/**
	 * @return the valueRetriever
	 */
	public ValueRetriever<Map<String, String>> getValueRetriever() {
		return valueRetriever;
	}


	/**
	 * @param valueRetriever the valueRetriever to set
	 */
	public void setValueRetriever(ValueRetriever<Map<String, String>> valueRetriever) {
		this.valueRetriever = valueRetriever;
	}

}
