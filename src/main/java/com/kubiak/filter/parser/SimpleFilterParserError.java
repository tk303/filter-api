package com.kubiak.filter.parser;

import java.text.ParseException;

/**
 * Parsing error thrown by {@link SimpleFilterParser} to indicate that expression cannot be processed
 * 
 * @author kubiak
 *
 */
public class SimpleFilterParserError extends ParseException {

	private static final long serialVersionUID = 1L;
	
	public SimpleFilterParserError(String s, int errorOffset) {
		super(s, errorOffset);
	}
}
