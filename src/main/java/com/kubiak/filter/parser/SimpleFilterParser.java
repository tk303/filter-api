package com.kubiak.filter.parser;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.List;
import java.util.stream.Collectors;

import com.kubiak.filter.Filter;

/**
 * Simple parser for expressions representing a Filter.
 * Every condition needs to be wrapped in a pair of parentheses, e.g.: (firstname = joe)
 * The only supported operation is currently equals ("=").
 * Supports AND, OR, to join expressions. Does not support NOT (negate).
 * 
 * When expression is wrongly formatted a {@link SimpleFilterParserError} will be thrown.
 * 
 * @author Tomasz Kubiak
 *
 */
public class SimpleFilterParser {

	/** internal array to store expressions string */
	private final char[] filterArray;

	/** Placing filters and join operation on stacks allows to handle nested expressions */
	Deque<Filter> filterStack;
	Deque<String> operationStack;

	/**
	 * Creates {@link SimpleFilterParser} for an expression to parse
	 * @param filterText expression to parse
	 */
	public SimpleFilterParser(String filterText) {
		this.filterArray = filterText.toCharArray();
		filterStack = new ArrayDeque<>();
		operationStack = new ArrayDeque<>();
	}

	/**
	 * Parses filter expression and returns a {@link Filter}
	 * 
	 * @return
	 * @throws SimpleFilterParserError
	 */
	public Filter parse() throws SimpleFilterParserError {

		int predicateStart, nextStart, predicateEnd;

		predicateStart = findPredicateStart(0);
		nextStart = predicateStart;
		predicateEnd = findPredicateEnd(predicateStart + 1);

		while (nextStart < filterArray.length && predicateEnd < filterArray.length) {

			String predicateText = String.copyValueOf(filterArray, predicateStart + 1, predicateEnd - (predicateStart +1));

			Filter filter;
			if(predicateText.contains("(")) {
				// Calls new parser recursively to handle nested expressions
				filter = new SimpleFilterParser(predicateText).parse();
			}
			else {
				// find last opening parenthesis
				while (nextStart < predicateEnd) {
					nextStart = findPredicateStart(nextStart + 1);
				}
				// split expression
				String[] predicatePieces = predicateText.split(" ");
				List<String> piecesList = Arrays.asList(predicatePieces).stream()
						.filter(text -> !text.isEmpty())
						.map(text -> text.trim())
						.collect(Collectors.toList());
	
				/* Three elements are expected to represent a correct condition:
				 *  1. A property name
				 *  2. Condition (AND, OR)
				 *  3. A property value  */
				if (piecesList.size() != 3)
					throw new SimpleFilterParserError("Number of predicate elements is different then 3", predicateStart);
	
				filter = new Filter();
				// switch depending on operation. Currently only equals operation supported
				switch (piecesList.get(1)) {
				case "=":
					filter.propertyEqual(piecesList.get(0), piecesList.get(2));
					break;
				default:
					throw new SimpleFilterParserError("Invalid predicate operation", predicateStart);
				}
				
			}
			filterStack.push(filter);
			nextStart = findPredicateStart(predicateEnd + 1);
			
			// check if there is joint operation 
            if( (nextStart - predicateEnd > 3)) {
    			String joinOperation = String.copyValueOf(filterArray, predicateEnd + 1,  nextStart - (predicateEnd +1));
    			joinOperation = joinOperation.trim();
    			operationStack.push(joinOperation);
            }
			
			predicateEnd = findPredicateEnd(nextStart + 1);
			predicateStart = nextStart;
			
		}
		
		// retrieve Filters from the stack and join them together with operations from operation stack
		while (!filterStack.isEmpty()) {
			
			Filter rightSideFilter = filterStack.pop(); 
			if(filterStack.isEmpty()) { 
				return rightSideFilter;
			}
			 
			String operation = operationStack.pop();
			Filter leftSideFilter = filterStack.pop();

			Filter jointFilter;
			switch (operation.toUpperCase()) {
			case "AND":
				jointFilter = leftSideFilter.and(rightSideFilter);
				filterStack.push(jointFilter);
				break;
			case "OR":
				jointFilter = leftSideFilter.or(rightSideFilter);
				filterStack.push(jointFilter);
				break;
			default:
				throw new SimpleFilterParserError("Invalid predicates join. Only AND, OR are allowed", -1);
			}
		}

		return null;
	}

	/**
	 * Returns the first encountered opening bracket - indicating a start of condition expression
	 * @param position
	 * @return
	 */
	private int findPredicateStart(int position) {
		while (position < filterArray.length && !isPredicateStart(filterArray[position])) {
			position++;
		}
		return position;
	}

	/**
	 * Checks whether character represents a start of conditions expression (opening parenthese).
	 * @param c
	 * @return 
	 */
	private boolean isPredicateStart(final char c) {
		if (c == '(') {
			return true;
		}
		return false;
	}

	/**
	 * Finds the position of a closing parenthese 
	 * Can handle nested parentheses
	 * @param position
	 * @return
	 */
	private int findPredicateEnd(int position) {
		// holds the amount of opening parentheses encountered
		int openingParenthese = 0;
		while (true) {
			if (position >= filterArray.length)
				return position;
			else {
				if (isPredicateStart(filterArray[position])) {
					openingParenthese++;
				}
				if (isPredicateEnd(filterArray[position])) {
					if (openingParenthese == 0) {
						return position;
					}
					openingParenthese--;
				}
			}
			position++;
		}
	}

	/**
	 * Checks whether character represents an end of expression (closing parenthese).
	 * @param c
	 * @return 
	 */
	private boolean isPredicateEnd(final char c) {
		return c == ')';
	}

}
