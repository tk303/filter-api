package com.kubiak.filter;

import java.util.LinkedHashMap;
import java.util.Map;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.kubiak.filter.Filter;

public class FilterTestComplex {
	
	private Filter filter;
	private Filter filterJoe;
	private Filter filterJoanne;
	
	private Map<String, String> userJoe;
	
	private Map<String, String> userJoanne;
	
	
	@Before
	public void setUp() {
		filter = new Filter();
		filterJoe = new Filter();
		filterJoanne = new Filter();
		userJoe = new LinkedHashMap<>();
		userJoe.put("firstname", "Joe");
		userJoe.put("surname", "Bloggs");
		userJoe.put("role", "administrator");
		userJoe.put("age", "35");
		userJoe.put("gender", "male");
		
		userJoanne = new LinkedHashMap<>();
		userJoanne.put("firstname", "Joanne");
		userJoanne.put("surname", "Phillips");
		userJoanne.put("role", "assistant");
		userJoanne.put("age", "42");
		userJoanne.put("gender", "female");
		
		filterJoe.propertyEqual("firstname", "Joe");
		filterJoe.propertyGreaterThan("age", 30);
		
		filterJoanne.propertyEqual("firstname", "Joanne");
		filterJoanne.propertyGreaterThan("age", 40);
	}
	
	@Test
	public void shouldMatchFilterJoe() {
		Assert.assertTrue(filterJoe.matches(userJoe));
	}
	
	@Test
	public void shouldMatchFilterJoanne() {
		Assert.assertTrue(filterJoanne.matches(userJoanne));
	}
	
	
	@Test
	public void shouldMatchComplexOrFilter() {
		// given
		
		// when
		filter = filterJoe.or(filterJoanne);
		
		// then
		Assert.assertTrue(filter.matches(userJoe));
		Assert.assertTrue(filter.matches(userJoanne));
	}
	
	@Test
	public void shouldNotMatchComplexAndFilter() {
		// given
		
		// when
		filter = filterJoe.and(filterJoanne);
		
		// then
		Assert.assertTrue(!filter.matches(userJoe));
		Assert.assertTrue(!filter.matches(userJoanne));
	}
	
	@Test
	public void shouldMatchJoeAndNotJoanne() {
		// given
		filterJoanne.negate();
		
		// when
		filter = filterJoe.and(filterJoanne);
		
		// then
		Assert.assertTrue(filter.matches(userJoe));
		Assert.assertTrue(!filter.matches(userJoanne));
	}
	
	@Test
	public void shouldMatchComplexNegateOrFilter() {
		// given
		filterJoanne.negate();
		
		// when
		filter = filterJoe.or(filterJoanne);
		
		// then
		Assert.assertTrue(filter.matches(userJoe));
		Assert.assertTrue(!filter.matches(userJoanne));
	}
	
	@Test
	public void shouldMatchComplexNegateAndFilter() {
		// given
		filterJoanne.negate();
		
		// when
		filter = filterJoe.and(filterJoanne);
		
		// then
		Assert.assertTrue(filter.matches(userJoe));
		Assert.assertTrue(!filter.matches(userJoanne));
	}
	
}
