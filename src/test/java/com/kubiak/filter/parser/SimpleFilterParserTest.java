package com.kubiak.filter.parser;

import java.util.LinkedHashMap;
import java.util.Map;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.kubiak.filter.Filter;

public class SimpleFilterParserTest {
	
	// under test
	SimpleFilterParser parser;
	private Map<String, String> resource;
	private Map<String, String> resourceNoMatch;
	
	@Before
	public void setup() {
		resource = new LinkedHashMap<>();
		resourceNoMatch = new LinkedHashMap<>();
	}

	@Test
	public void testSimpleExpression() throws SimpleFilterParserError {
		// given
		String expression = "(surname = smith)";
		resource.put("surname", "smith");
		resourceNoMatch.put("surname", "gray");
		
		//when
		parser = new SimpleFilterParser(expression);
		Filter filter = parser.parse();
		
		// then
		Assert.assertTrue(filter.matches(resource));
		Assert.assertTrue(!filter.matches(resourceNoMatch));
	}
	
	@Test
	public void testTwoSimpleExpression() throws SimpleFilterParserError {
		// given
		String expression = "(surname = smith) and (firstname = joe)";
		resource.put("surname", "smith");
		resource.put("firstname", "joe");
		resourceNoMatch.put("surname", "gray");
		
		//when
		parser = new SimpleFilterParser(expression);
		Filter filter = parser.parse();
		
		// then
		Assert.assertTrue(filter.matches(resource));
		Assert.assertTrue(!filter.matches(resourceNoMatch));
	}
	
	@Test
	public void testThreeExpressions() throws SimpleFilterParserError {
		// given
		String expression = "((surname = smith) and (firstname = joe)) or (surname = jones)";
		resource.put("surname", "smith");
		resource.put("firstname", "joe");
		resourceNoMatch.put("surname", "gray");
		
		// additional to check the or operation working
		Map<String, String> resource2ndMatch = new LinkedHashMap<>();
		resource2ndMatch.put("surname", "jones");
		
		//when
		parser = new SimpleFilterParser(expression);
		Filter filter = parser.parse();
		
		// then
		Assert.assertTrue(filter.matches(resource));
		Assert.assertTrue(filter.matches(resource2ndMatch));
		Assert.assertTrue(!filter.matches(resourceNoMatch));
	}
}
