package com.thirdparty;

import java.util.LinkedHashMap;
import java.util.Map;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.kubiak.filter.Filter;

public class AdministratorOnlyTest {
	
	private Filter filter;
	
	private Map<String, String> simpleUser;
	private Map<String, String> administratorUser;
	
	
	@Before
	public void setUp() {
		filter = new AdministratorOnly();
		simpleUser = new LinkedHashMap<>();
		simpleUser.put("firstname", "Joe");
		simpleUser.put("surname", "Bloggs");
		simpleUser.put("age", "35");
		
		administratorUser = new LinkedHashMap<>();
		administratorUser.put("firstname", "Tomasz");
		administratorUser.put("surname", "Kubiak");
		administratorUser.put("role", "administrator");
		administratorUser.put("age", "31");
	}
	
	@Test
	public void shouldReturnTrueForAdminUser() {
		// given
		
		// when
		filter.propertyEqual("firstname", "Tomasz");
		filter.propertyGreaterThan("age", 30);
		
		// then
		Assert.assertTrue(filter.matches(administratorUser));
	}
	
	@Test
	public void shouldReturnFalseForNonAdminUser() {
		// given
		
		// when
		filter.propertyEqual("firstname", "Joe");
		filter.propertyGreaterThan("age", 30);
		
		// then
		Assert.assertTrue(!filter.matches(simpleUser));
	}
}
