package com.thirdparty;

import java.util.LinkedHashMap;
import java.util.Map;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.kubiak.filter.Filter;

public class FilterIgnoreCaseTest {
	
	private FilterIgnoreCase filter;
	
	private Map<String, String> sampleUser;
	
	
	@Before
	public void setUp() {
		filter = new FilterIgnoreCase();
		sampleUser = new LinkedHashMap<>();
		sampleUser.put("firstname", "Joe");
		sampleUser.put("surname", "Bloggs");
		sampleUser.put("role", "administrator");
		sampleUser.put("age", "35");
		sampleUser.put("premiumUser", "true");
	}
	
	@Test
	public void propertyCaseShouldNotMatter() {
		// given
		
		// when
		filter.propertyEqual("firstname", "joe");
		
		// then
		Assert.assertTrue(filter.matches(sampleUser));
	}
	
	
	/* ************ original Filter tests ************** */
	
	@Test
	public void shouldReturnTrueWhenIntPropertyEqual() {
		// given
		
		// when
		filter.propertyEqual("age", 35);
		
		// then
		Assert.assertTrue(filter.matches(sampleUser));
	}
	
	@Test
	public void shouldReturnTrueWhenIntPropertyGreater() {
		// given
		
		// when
		filter.propertyGreaterThan("age", 30);
		
		// then
		Assert.assertTrue(filter.matches(sampleUser));
	}
	
	@Test
	public void shouldReturnTrueWhenIntPropertyLess() {
		// given
		
		// when
		filter.propertyLessThan("age", 100);
		
		// then
		Assert.assertTrue(filter.matches(sampleUser));
	}
	
	@Test
	public void shouldReturnTrueForBooleanProperty() {
		// given
		
		// when
		filter.propertyIsTrue("premiumUser");
		
		// then
		Assert.assertTrue(filter.matches(sampleUser));
	}
	
	@Test
	public void shouldReturnFalseForFalseBooleanProperty() {
		// given
		sampleUser.put("premiumUser", "false");
		
		// when
		filter.propertyIsTrue("premiumUser");
		
		// then
		Assert.assertTrue(!filter.matches(sampleUser));
	}
	
	@Test
	public void shouldReturnFalseForBooleanConversionError() {
		// given
		
		// when
		filter.propertyIsTrue("age");
		
		// then
		Assert.assertTrue(!filter.matches(sampleUser));
	}
	
	@Test
	public void shouldReturnTrueForPatternMatch() {
		// given
		
		// when
		filter.propertyMatches("surname", "logs");
		
		// then
		Assert.assertTrue(filter.matches(sampleUser));
	}
	
	@Test
	public void shouldReturnDefaultWhenError() {
		// given
		boolean defaultWhenError = true;
		filter.setDefaultWhenError(defaultWhenError);
		
		// when
		filter.propertyLessThan("firstname", 100); // firstname ("Joe") can't be converted to int
		
		// then
		Assert.assertEquals(defaultWhenError, filter.matches(sampleUser));
	}
	
	@Test
	public void shouldReturnTrueForTwoTruePredicates() {
		// given
		
		// when
		filter.propertyLessThan("age", 100);
		filter.propertyEqual("firstname", "Joe");
		
		// then
		Assert.assertTrue(filter.matches(sampleUser));
	}
	
	@Test
	public void shouldReturnTrueForTwoWithGreaterThan() {
		// given
		
		// when
		filter.propertyEqual("firstname", "Joe");
		filter.propertyGreaterThan("age", 30);
		
		// then
		Assert.assertTrue(filter.matches(sampleUser));
	}
	
	@Test
	public void shouldReturnFalseForTrueAndFalsePredicate() {
		// given
		
		// when
		filter.propertyGreaterThan("age", 100);
		filter.propertyEqual("firstname", "Joe");
		
		// then
		Assert.assertTrue(!filter.matches(sampleUser));
	}
	
	@Test
	public void shouldAllowToModifyDefaultWhenMissingAfterAddingPredicates() {
		// given
		boolean defaultWhenMissing = true;
		filter.setDefaultWhenMissing(defaultWhenMissing);
		
		// when
		filter.propertyLessThan("age", 100);
		filter.propertyEqual("firstname", "Joe");
		filter.propertyEqual("nonExisting", "some-value"); // add non existing property
		
		defaultWhenMissing = false;
		filter.setDefaultWhenMissing(defaultWhenMissing); // change default behaviour
		
		
		// then
		Assert.assertEquals(defaultWhenMissing, filter.matches(sampleUser));
	}
	
	@Test
	public void shouldAllowToModifyDefaultOnErrorAfterAddingPredicates() {
		// given
		boolean defaultWhenMissing = true;
		filter.setDefaultWhenMissing(defaultWhenMissing);
		boolean defaultWhenError = true;
		filter.setDefaultWhenError(defaultWhenError);
		
		// when
		filter.propertyLessThan("age", 100);
		filter.propertyEqual("firstname", "Joe");
		filter.propertyEqual("nonExisting", "some-value"); // add non existing property
		filter.propertyGreaterThan("surname", 20); // surname can't be converted and will throw an error
		
		defaultWhenMissing = false;
		filter.setDefaultWhenMissing(defaultWhenMissing); // change default behaviour
		
		// then
		Assert.assertEquals(defaultWhenMissing, filter.matches(sampleUser));
	}
	
	@Test
	public void shouldReturnDefaultWhenNoPredicates() {
		// given
		boolean defaultWhenNoPredicate = true;
		filter.setDefaultWhenNoPredicate(defaultWhenNoPredicate);
		
		// when
		// no predicates set
		
		// then
		Assert.assertEquals(defaultWhenNoPredicate, filter.matches(sampleUser));
	}
	
	
	@Test
	public void shouldAssertCorrectlyForTestProvided() {
		// Create a filter which matches all administrators older than 30:
		Filter filter = new Filter(); // Create a filter using your API.
		filter.propertyEqual("role", "administrator");
		filter.propertyGreaterThan("age", 30);
		Assert.assertTrue(filter.matches(sampleUser)); // Filter should match.
		sampleUser.put("age", "25");
		Assert.assertTrue(!filter.matches(sampleUser)); // Filter should not match.
	}
}
