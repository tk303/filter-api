package com.thirdparty;

import java.util.LinkedHashMap;
import java.util.Map;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.kubiak.filter.Filter;

public class ErrorPropagatorTest {
	
	private Filter filter;
	
	private Map<String, String> sampleUser;
	
	@Before
	public void setUp() {
		filter = new Filter();
		filter.setWrapperClass(ErrorPropagator.class);
		
		sampleUser = new LinkedHashMap<>();
		sampleUser.put("firstname", "Joe");
		sampleUser.put("surname", "Bloggs");
		sampleUser.put("age", "35");
	}
	
	@Test(expected = NumberFormatException.class)
	public void shouldReturnTrueForYoungerThan() {
		// given
		
		// when
		filter.propertyLessThan("firstname", 30);
		
		// then
		Assert.assertTrue(filter.matches(sampleUser));
	}
	
	@Test
	public void shouldReturnTrue() {
		// given
		
		// when
		filter.propertyEqual("firstname", "Joe");
		
		// then
		Assert.assertTrue(filter.matches(sampleUser));
	}
}
