package com.thirdparty;

import java.util.LinkedHashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SimpleAgeFilterTest {
	
	private SimpleAgeFilter filter;
	
	private Map<String, String> sampleUser;
	
	
	@Before
	public void setUp() {
		filter = new SimpleAgeFilter();
		sampleUser = new LinkedHashMap<>();
		sampleUser.put("firstname", "Joe");
		sampleUser.put("surname", "Bloggs");
		sampleUser.put("age", "35");
	}
	
	@Test
	public void shouldReturnTrueForYoungerThan() {
		// given
		
		// when
		filter.youngerThan(40);
		
		// then
		Assert.assertTrue(filter.matches(sampleUser));
	}
	
	@Test
	public void shouldReturnTrueForOlderThan() {
		// given
		
		// when
		filter.propertyEqual("firstname", "Joe");
		filter.olderThan(20);
		
		// then
		Assert.assertTrue(filter.matches(sampleUser));
	}
}
